#!/bin/sh

MAVEN_VERSION=3.6.0
JAVA_VERSION=8

docker build -t maven:${MAVEN_VERSION}-jdk-${JAVA_VERSION}-onbuild --build-arg MAVEN_VERSION=${MAVEN_VERSION} --build-arg JAVA_VERSION=${JAVA_VERSION} .
docker tag maven:${MAVEN_VERSION}-jdk-${JAVA_VERSION}-onbuild nexus.local:8083/maven:${MAVEN_VERSION}-jdk-${JAVA_VERSION}-onbuild