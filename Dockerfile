FROM nexus.local:8083/maven:3.6.0-jdk-8-onbuild as builder

FROM nexus.local:8083/springboot:jdk-8-runtime

LABEL maintainer=rschmid@smartwavesa.com
