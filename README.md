# docker-springboot-helloworld

## Prerequisites

run onbuild/maven/build.sh



```
docker tag maven:3.6.0-jdk-8-onbuild nexus.local:8083/maven:3.6.0-jdk-8-onbuild
docker push nexus.local:8083/maven:3.6.0-jdk-8-onbuild
```

run runtim/maven/build.sh
```
docker tag springboot:jdk-8-runtime nexus.local:8083/springboot:jdk-8-runtime
docker push nexus.local:8083/springboot:jdk-8-runtime
```

## Build springboot-helloworld
```
docker build -t springboot-helloworld-app:v1 .
```
