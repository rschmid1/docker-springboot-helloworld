#!/bin/sh
JAVA_VERSION=8

docker build -t springboot:jdk-${JAVA_VERSION}-runtime --build-arg JAVA_VERSION=${JAVA_VERSION} .
docker tag springboot:jdk-${JAVA_VERSION}-runtime nexus.local:8083/springboot:jdk-${JAVA_VERSION}-runtime